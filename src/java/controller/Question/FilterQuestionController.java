/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Question;

import dao.DimensionDAO;
import dao.LessonDAO;
import dao.QuestionDAO;
import dao.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Question;
import model.QuestionL;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "FilterQuestionController", urlPatterns = {"/FilterQuestion"})
public class FilterQuestionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
//            HttpSession session = request.getSession();           
//            Account a = (Account) session.getAttribute("account");
            int status = Integer.parseInt(request.getParameter("statusFilter") != null ? request.getParameter("statusFilter") : "2");
            int subjectId = Integer.parseInt(request.getParameter("subjectIdFilter") != null ? request.getParameter("subjectIdFilter") : "0");
            int lessonId = Integer.parseInt(request.getParameter("lessonIdFilter") != null ? request.getParameter("lessonIdFilter") : "0");
            int dimId = 0;
            ArrayList<QuestionL> questionList = null;
            if (request.getParameter("statusFilter") != null) {
                questionList = new QuestionDAO().getQuestionByStatus(status, subjectId, lessonId, dimId);
                if (!questionList.isEmpty()) {
                    request.setAttribute("listQuestion", questionList);
                } else {
                    request.setAttribute("message", "Not have question!");
                }
            }

            request.setAttribute("status", status);
            request.setAttribute("subjectId", subjectId);
            request.setAttribute("listSubject", new SubjectDAO().getAllSubjects());
            request.setAttribute("lessonId", lessonId);
            request.setAttribute("listLesson", new LessonDAO().getListLessons());
            request.setAttribute("dimId", dimId);
            request.setAttribute("listDimension", new DimensionDAO().getAllDimension());

            request.getRequestDispatcher("QuestionList.jsp").forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
