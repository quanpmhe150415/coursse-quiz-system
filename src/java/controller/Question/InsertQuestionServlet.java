/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Question;

import dao.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Question;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "InsertQuestion", urlPatterns = {"/InsertQuestion"})
public class InsertQuestionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String action = request.getParameter("action");
        if (action == null) {
            
        } else if (action.equals("insert")) {
            doPost(request, response);
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "";
        try {
            int subId = Integer.parseInt(request.getParameter("subId"));
            int quizId = Integer.parseInt(request.getParameter("quizId"));
            int lessonId = Integer.parseInt(request.getParameter("lessonId"));
            String content = request.getParameter("content");
            boolean isMultiple = false;
            if (request.getParameter("isMultiple") == null) {
                isMultiple = false;
            } else {
                isMultiple = true;
            }

            Question question = new Question(1, content, subId, lessonId, 1, "", true, quizId, "", isMultiple);

            QuestionDAO questionDAO = new QuestionDAO();
            boolean insertQuestion = questionDAO.insertQuestion(question);
            if (insertQuestion) {
                Question lastQuestion = questionDAO.getLastestQuestion();
                url = "question-detail?questionId=" + lastQuestion.getQuestionId();
                response.sendRedirect(url);
                return;
            } else {
                url = "QuestionListAdminServlet?quizId=" + quizId + "&subId=" + subId;
                request.getRequestDispatcher(url).forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
