/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Answer;

import dao.AnswerDAO;
import dao.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Answer;
import model.AnswerD;
import model.Question;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "UpdateAnswerServlet", urlPatterns = {"/updateAnswer"})
public class UpdateAnswerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = "";
        try {
            String action = request.getParameter("action");
            int QuestionId = Integer.parseInt(request.getParameter("questionId"));
            AnswerDAO answerDAO = new AnswerDAO();
            Question question = new QuestionDAO().getQuestionById(QuestionId);
            if (action == null) {
                url = "Login.jsp";
            } else if (action.equals("Add")) {
                //Do Add
                String content = request.getParameter("answerContent");
                AnswerD answer = new AnswerD(0, content, Boolean.valueOf(request.getParameter("isCorrect")), QuestionId);
                answerDAO.insertAnswer(answer);
                url = "question-detail?questionId=" + QuestionId;
                response.sendRedirect(url);
            } else if (action.equals("Update")) {
                int answerId = Integer.parseInt(request.getParameter("answerId"));
                String content = request.getParameter("answerContent");
                boolean correct = false;

                if (request.getParameter("isCorrect").equals("true")) {
                    correct = true;
                } else {
                    correct = false;
                }
                answerDAO.updateAnswer(answerId, content, correct);
                url = "question-detail?questionId=" + QuestionId;
                response.sendRedirect(url);
            } else if (action.equals("Delete")) {
                int answerId = Integer.parseInt(request.getParameter("answerId"));
                answerDAO.deleteAnswer(answerId);
                url = "question-detail?questionId=" + QuestionId;
                response.sendRedirect(url);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
