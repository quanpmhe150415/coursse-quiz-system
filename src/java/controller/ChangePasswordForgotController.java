/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "ChangePasswordForgotController", urlPatterns = {"/ChangePasswordForgot"})
public class ChangePasswordForgotController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String id = request.getParameter("txtID");

        if (id == null) {
            request.setAttribute("WARNING", "You can't perform this action");
            request.getRequestDispatcher("change-password-forgot.jsp").forward(request, response);
        } else {
//           request.setCharacterEncoding("utf-8");
            AccountDAO accountDAO = new AccountDAO();
            Account account = accountDAO.getAccountById(Integer.parseInt(id));
            if (account != null) {
                request.setAttribute("ACCOUNT", account);
                request.getRequestDispatcher("change-password-forgot.jsp").forward(request, response);
            } else {
                request.setAttribute("WARNING", "You can't perform this action");
                request.getRequestDispatcher("change-password-forgot.jsp").forward(request, response);
            }

        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String token = request.getParameter("txtToken");
        String newPass = request.getParameter("new-password");
        String rePass = request.getParameter("confirm-password");
        if(!newPass.equals(rePass)){
            request.setAttribute("WARNING", "Password and Re-Pass is not match");
            request.getRequestDispatcher("change-password-forgot.jsp").forward(request, response);
            return;
        }
        int userId = Integer.parseInt(request.getParameter("txtUserId"));

        AccountDAO accountDAO = new AccountDAO();
        boolean isChange = accountDAO.updateNewPasswordForgoted("" + userId, newPass);
        if (isChange) {
            request.setAttribute("WARNING", "Password has been changed succesfully!");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        } else {
            request.setAttribute("WARNING", "Something is wrong!");
            request.getRequestDispatcher("change-password-forgot.jsp").forward(request, response);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
