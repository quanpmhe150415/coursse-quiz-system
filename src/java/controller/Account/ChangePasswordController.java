/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Account;

import dao.AccountDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "ChangePasswordController", urlPatterns = {"/ChangePassword"})
public class ChangePasswordController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        request.getRequestDispatcher("change-password.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");

        AccountDAO accountDao = new AccountDAO();
        Account oldAccount = (Account) request.getSession().getAttribute("account");
        String oldPass = request.getParameter("oldPass");
        if(!oldPass.equals(oldAccount.getPassword())){
            request.setAttribute("Warning", "Old password is wrong!");
            request.getRequestDispatcher("change-password.jsp").forward(request, response);
            return;
        }
        String newPassword = request.getParameter("newPassword");
        String repass = request.getParameter("re-pass");
        if(!repass.equals(newPassword)){
            request.setAttribute("Warning", "Password not match with repass!");
            request.getRequestDispatcher("change-password.jsp").forward(request, response);
            return;
        }
        int userid = oldAccount.getUserid();
        
        accountDao.changePassword(userid, newPassword);
        String isNoti = "yes";
        request.setAttribute("isNoti", isNoti);

        oldAccount.setPassword(newPassword);
        request.getSession().setAttribute("account", oldAccount);
        //request.getRequestDispatcher("home.jsp").forward(request, response);
        response.sendRedirect("profile");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
