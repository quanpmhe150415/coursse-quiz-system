/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Account;

import Base.Base;
import dao.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.SendEmail;
import testU.generator;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "ResetPassworldController", urlPatterns = {"/reset"})
public class ResetPassworldController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("reset-password.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");

        boolean isExistAccount = new AccountDAO().isExistAccount(email);
        int id = new AccountDAO().getAccountIdByEmail(email);
        if (!isExistAccount) {
            request.setAttribute("error", "Email is not exist. Please try again!");
        } else {
            String subject = "Change your password";
            String message = "<!DOCTYPE html>\n"
                    + "<html lang=\"en\">\n"
                    + "<head></head>\n"
                    + "<body style=\"color:#000;\">\n"
                    + "    <h3>Quiz Pracite system</h3>\n"
                    + "    <p>Please click here to change your password| The Code Valid in 5 Minutes</p>\n"
                    //                        + "    <form id=\"myForm\" method=\"POST\" action=" + Base.LINK_CHANGE_PASSWORD + ">\n"
                    //                        + "        <input type=\"hidden\" value=" + email + " id=\"email\" name=\"email\">\n"
                    //                        + "        <input type=\"submit\" value=\"Change password\" \n"
                    //                        + "            style=\"padding: 10px 15px;color: #fff;background-color: rgb(0, 149, 255);border-radius: 10px;border:none\">\n"
                    //                        + "    </form>\n"
                    + "<a href=\"http://localhost:8080/system/ChangePasswordForgot?txtID=" + id + "\""
                    + "style=\"padding: 10px 15px;color: #fff;background-color: rgb(0, 149, 255);border-radius: 10px;border:none\">Change Password</a>"
                    + "    <h4>Thank you very much</h4>\n"
                    + "</body>\n"
                    + "</html>";

            SendEmail.sendMail(email, subject, message, Base.USERNAME_EMAIL, Base.PASSWORD_EMAIL);
            request.setAttribute("success", "Change password link has been sent to your email");
        }
        request.getRequestDispatcher("reset-password.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
