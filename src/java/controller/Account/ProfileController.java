/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Account;

import dao.AccountDAO;
import java.io.File;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileNotFoundException;
import model.Account;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "ProfileController", urlPatterns = {"/profile"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 10,
        maxFileSize = 1024 * 1024 * 10,
        maxRequestSize = 1024 * 1024 * 10
)
public class ProfileController extends HttpServlet {

    private static final long SerialVersionUID = 1L;
    private static final String UPLOAD_DIR = "img";
    public static final String DEFAULT_FILENAME = "default.file";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        System.out.println("session: " + request.getSession().getAttribute("account"));
        Account account = (Account) request.getSession().getAttribute("account");
        System.out.println("acc: " + account);
        request.setAttribute("acc", request.getSession().getAttribute("account"));
        request.getRequestDispatcher("profile.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        AccountDAO accountDAO = new AccountDAO();
        String isNoti = "yes";
        request.setAttribute("isNoti", isNoti);
        Account oldAccount = (Account) request.getSession().getAttribute("account");
        int accountID = oldAccount.getUserid();//lay id cu
        String fullname = request.getParameter("fullname");
        //String lastName = request.getParameter("lastName");
//        boolean gender = true;
//        if (request.getParameter("gender").equalsIgnoreCase("female")) {
//            gender = false;
//        }
        int phone = Integer.parseInt(request.getParameter("phone"));
        String address = request.getParameter("address");
        String filename = uploadFile(request);
        Account acc = (Account) request.getSession().getAttribute("account");
        // System.out.println("Phone: "+phone);
        // System.out.println("Name: "+fullname);
        // System.out.println("address: "+address);
//        acc.setUserid(accountID);
        acc.setFullname(fullname);
        //acc.setLastName(lastName);
        //acc.setGender(Gender.of(gender));
        acc.setPhone(phone);
        acc.setAddress(address);
        if (!filename.isEmpty()) {
            acc.setAvatar(filename);
        }
        // acc.setAvatar(filename);
        accountDAO.editProfile(acc);

        Account accountUpdate = accountDAO.getAccount(oldAccount.getEmail(), oldAccount.getPassword());
//        String a = oldAccount.getUsername();
        request.getSession().setAttribute("account", accountUpdate);
        response.sendRedirect("profile");
    }

    public String uploadFile(HttpServletRequest request) throws IOException, ServletException {
        String fileName = "";
        int length = getServletContext().getRealPath("/").length();
        String uploadPath = new StringBuilder(getServletContext().getRealPath("/")).delete(length - 10, length - 4).toString() + File.separator + "uploads";
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
        try {
            for (Part part : request.getParts()) {
                String temp = getFileName(part);
                if (!temp.equals(DEFAULT_FILENAME) && !temp.trim().isEmpty()) {
                    fileName = temp;
                    part.write(uploadPath + File.separator + fileName);
                    break;
                }
            }
        } catch (FileNotFoundException fne) {
        }
        return fileName;
    }

    private String getFileName(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf("=") + 2, content.length() - 1);
            }
        }
        return DEFAULT_FILENAME;
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
