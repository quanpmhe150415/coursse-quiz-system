/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Subject;

import static controller.Account.ProfileController.DEFAULT_FILENAME;
import dao.CategoryDAO;
import dao.SubjectDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.FileNotFoundException;
import model.Account;
import model.Subject;

/**
 *
 * @author Administrator
 */
@WebServlet(name = "EditSubjectController", urlPatterns = {"/EditSubjectController"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 10,
        maxFileSize = 1024 * 1024 * 50,
        maxRequestSize = 1024 * 1024 * 100
)
public class EditSubjectController extends HttpServlet {

    private static final long SerialVersionUID = 1L;
    private static final String UPLOAD_DIR = "img";
    public static final String DEFAULT_FILENAME = "default.file";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            Account u = (Account) session.getAttribute("account");
//            if (u != null && u.getRoleid() == 1) {
            String image = uploadFile(request);
            String subId = request.getParameter("id");
            System.out.println("SubId in edit controller: " + subId);
            String subjectName = request.getParameter("name");
            String description = request.getParameter("description");
            int tagLine = 20;
            boolean status = Boolean.parseBoolean(request.getParameter("status"));
            String title = request.getParameter("title");
            int catId = 1;
            SubjectDAO subDao = new SubjectDAO();
            Subject sub = subDao.getSubjectById(subId);
            System.out.println("Subject have id " + subId + ": " + sub.getSubjectName());
            request.setAttribute("s", sub);
            if (request.getParameter("name") == null) {
                subjectName = sub.getSubjectName();
            }
            if (request.getParameter("status").isEmpty()) {
                status = sub.isStatus();
            }
            if (request.getParameter("tagLine") != null) {
                tagLine = sub.getTagLine();
            }
            if (title.isEmpty()) {
                title = sub.getTitle();
            }
            if (image.isEmpty()) {
                image = sub.getThumbnail();
            }
            if (description.isEmpty()) {
                description = sub.getDescription();
            }
            String message = "";
            Subject subject = new Subject(Integer.parseInt(subId), subjectName, catId, status, tagLine, title, image, description);
            if (subDao.Update(subject)) {
                message = "Update sucessfull";
            } else {
                message = "Update failed";
            }
            System.out.println("message: " + message);
            request.setAttribute("message", message);
            request.getRequestDispatcher("SubjectDetailAdminController?id=" + subId).forward(request, response);

//            }
//          else {
//                request.setAttribute("message", "Login first.");
//                request.getRequestDispatcher("Login.jsp").forward(request, response);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    public String uploadFile(HttpServletRequest request) throws IOException, ServletException {
        String fileName = "";
        int length = getServletContext().getRealPath("/").length();
        String uploadPath = new StringBuilder(getServletContext().getRealPath("/")).delete(length - 10, length - 4).toString() + File.separator + "uploads";
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
        try {
            for (Part part : request.getParts()) {
                String temp = getFileName(part);
                if (!temp.equals(DEFAULT_FILENAME) && !temp.trim().isEmpty()) {
                    fileName = temp;
                    part.write(uploadPath + File.separator + fileName);
                    break;
                }
            }
        } catch (FileNotFoundException fne) {
        }
        return fileName;
    }

    private String getFileName(Part part) {
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf("=") + 2, content.length() - 1);
            }
        }
        return DEFAULT_FILENAME;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
