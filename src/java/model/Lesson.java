/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor

/**
 *
 * @author Administrator
 */
public class Lesson {

    private int lessonId;
    private String lessonName;
    private String typeId;
    private int order;
    private String video_url;
    private String content;
    private int topicId;
    private Boolean status;
    private int subId;
    private String description;
    private String typeName;
}
