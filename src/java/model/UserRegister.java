/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor

/**
 *
 * @author Administrator
 */
public class UserRegister {

    private int userId;
    private int regisId;
    private String regisStatis;
    private int subjectId;
    private String subjectName;
    private int categoryId;
    private boolean status;
    private int tagLine;
    private String title;
    private String thumbnail;
    private String description;
    private int salePrice;
    private int price;
}
