/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
@AllArgsConstructor

/**
 *
 * @author Administrator
 */
public class Post {

    private int postId;
    private String thumbnail;
    private int userId;
    private int categoryBlogId;
    private String content;
    private Date created_date;
    private String edit_date;
    private Boolean status;
    private String brifInfor;
    private String title;
    private int postFileId;
}
