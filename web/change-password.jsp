<%-- 
    Document   : change-password
    Created on : Jun 23, 2023, 8:22:18 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <jsp:include page="base-view/baseTag.jsp"></jsp:include>       
            <link href="css/base.css" rel="stylesheet" type="text/css"/>
            <link href="css/change-password.css" rel="stylesheet" type="text/css"/>

            <title>Change password</title>

        </head>

        <body>

        <jsp:include page="base-view/headerUser.jsp"></jsp:include>       

            <div id="container">
                <form action="ChangePassword" method="POST" onsubmit="">
                    <div id="table-header">
                        <span>Change password</span>
                    </div>

                    <div>
                        <table>   



                            <tr>
                                <td>Current password</td>
                                <td><input type="password" name="oldPass" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"/></td>
                            </tr>
                            <tr><td colspan="2" id="error-current-password" class="error"></td></tr>

                            <tr>
                                <td>New password</td>
                                <td><input type="password" name="newPassword" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  value="" id="password"/></td>
                            </tr>
                            <tr><td colspan="2" id="error-password" class="error"></td></tr>

                            <tr>
                                <td>Confirm password</td>
                                <td><input type="password" name="re-pass" value="" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" id="re-password"/></td>
                            </tr>
                            <tr><td colspan="2" id="error-re-password" class="error">${requestScope.Warning}</td></tr>
                            <tr>
                                <td colspan="2"><input id="submit-btn" type="submit" value="Save" /></td>
                            </tr>
                        </table>
                    </div>
                </form>
    </body>
</html>
